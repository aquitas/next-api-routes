import Head from 'next/head'
import { useState, useEffect } from 'react'
import axios from 'axios'
import Link from 'next/link'

export default function Home() {
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(null)
  const [posts, setPosts] = useState([])

  useEffect(() => {
    const fetchData = async() => {
      try {
        const response = await axios.get(`http://localhost:3000/api`) 
        setLoading(false)
        setError(null)
        setPosts(response.data.posts)
      } catch (error) {
        console.error(error)
        setLoading(false)
        setError(error)
      }
    }

    fetchData()
  }, [])

  const renderData = () => {
    if(loading) {
      return <h1>...Loading...</h1>
    }

    if(error) {
      return <h1>An error occurred</h1>
    }

    return (
      <>
        <h1>Posts</h1>
        <ul>
          {
            posts.map((post) => {
              return (
                <li key={post.id}>
                  <Link href='/[id]' as={`/${post.id}`}>
                    <a>
                      {post.id}. {post.title}
                    </a>
                  </Link>
                </li>  
              )
            })
          }
        </ul>
      </>
    )
  }

  return (
    <>
      <Head>
        <title>Posts</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>{renderData()}</main>
    </>
  )
}

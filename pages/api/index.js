import { query } from '../../utils/db'

export default async (req, res) => {
  const { method } = req

  switch(method) {
    case('GET'):
      try {
        const posts = await query(`SELECT * FROM posts;`)
        return res.status(200).json({ mesage: 'Posts fetch successfull', posts })
      } catch(error) {
        console.log(error)
        return res.status(500).json({ message: 'An error occurred'})
      }
    default:
      return res.status(500).json({ message: `Unhandled method ${method}` })
  }
}
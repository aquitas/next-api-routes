import { query } from '../../utils/db'

export default async (req, res) => {
  const { method } = req

  switch(method) {
    case('GET'):
      try {
        const { query: { id } } = req
        const posts = await query(`SELECT * FROM posts WHERE id = ?;`, [id])
        return res.status(200).json({ mesage: 'Post fetch successfull', post: posts[0] })
      } catch(error) {
        console.log(error)
        return res.status(500).json({ message: 'An error occurred'})
      }
    default:
      return res.status(500).json({ message: `Unhandled method ${method}` })
  }
}
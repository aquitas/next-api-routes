import Head from 'next/head'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { useRouter } from 'next/router'

export default function Home() {
  const router = useRouter()

  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(null)
  const [post, setPost] = useState([])

  useEffect(() => {
    const fetchData = async() => {
      try {
        const { query: { id } } = router

        if (id) {
          const response = await axios.get(`http://localhost:3000/api/${id}`) 
          setLoading(false)
          setError(null)
          setPost(response.data.post)
        }
      } catch (error) {
        console.error(error)
        setLoading(false)
        setError(error)
      }
    }

    fetchData()
  }, [router])

  const renderData = () => {
    if(loading) {
      return <h1>...Loading...</h1>
    }

    if(error) {
      return <h1>An error occurred</h1>
    }

    return (
      <>
        <h1>{post.title}</h1>
        {post.content}
      </>
    )
  }

  return (
    <>
      <Head>
        <title>Posts</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>{renderData()}</main>
    </>
  )
}

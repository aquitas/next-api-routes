import mysql from 'serverless-mysql'

export const db = mysql({
  config: {
    host: 'localhost',
    database: 'next_api_routes',
    user: '',
    password: ''
  }
})

export const query = async (query, values) => {
  try {
    const response = await db.query(query, values)
    db.quit()
    return response
  } catch(error) {
    console.error(error)
  }
}